<!DOCTYPE html> 
<html lang="ru"> 
<head>	
	<meta charset="utf-8">
	<link rel="shortcut icon" href="https://cdn-icons.flaticon.com/png/512/2100/premium/2100130.png?token=exp=1641492935~hmac=a18513fa4f2cd6fd39f790b5128f6bca">
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<title>Главная страница</title>
</head> 
<body> 
	<header class="bg-primary bg-gradient">
		<div class="container pb-4">
			<div class="row">
				<div class="col-12">
					<h1 class="text-center text-white pt-3">Средняя температура</h1>
				</div>
			</div>
		</div>
	</header>
	<div class="container">
		<div class="row">
			<menu class="col-xl-2 col-md-6 col-sm-12 pt-5 pb-5 bg-light fw-bold">
				<p class="border-bottom">Датчики</p>
				<p class="px-4 border-bottom"><a href="sensor_list.php" class="text-decoration-none text-reset">Список датчиков</a></p>
				<p class="px-4 border-bottom"><a href="adding_sensor.php" class="text-decoration-none text-reset">Добавить датчик</a></p>
				<p class="border-bottom">Отчет</p>
				<p class="px-4 border-bottom"><a href="generate_report.php" class="text-decoration-none text-reset">Сформировать</a></p>
				<p class="px-4 border-bottom"><a href="report_history.php" class=" text-decoration-none text-reset">История</p>
			</menu>
			<section class="col-xl-10 col-md-6 col-sm-12">
				<!-- Секция для вывода информации -->
			</section>
		</div>
	</div>
	<footer class="footer fixed-bottom bg-secondary bg-gradient">
		<div class="container pb-5">	
		</div>
	</footer>
</body>
</html>