<?php
	
 	class DatabaseClass{

 		private $connection = null;

 		// this function is called everytime this class is instantiated		
        public function __construct($dbhost = "mysql", $port = "3306", $dbname = "averagetemperature", $username = "root", $password = "root"){

        	try{

        		$this->connection = new PDO("mysql:host={$dbhost};port={$port};dbname={$dbname};", $username, $password);
        		$this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        		$this->connection->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

        	} catch(Exception $exception){
        		throw new Exception($exception->getMessage());
        	}

        }

        // Insert a row/s in a Database Table
        public function Insert($statement = "", $parameters=[]){
        	try{

        		$this->executeStatement($statement, $parameters);
        		return $this->connection->lastInsertId();

        	}catch(Exception $exception){
        		throw new Exception($exception->getMessage());
        	}
        }

        // Select a row/s in a Database Table
        public function Select($statement = "", $parameters = []){
            try{

            	$stmt = $this->executeStatement($statement, $parameters);
            	return $stmt->fetchAll();

            }catch(Exception $exception){
            	throw new Exception($exception->getMessage());
            }
        }

        // Update a row/s in a Database Table
        public function Update($statement = "" , $parameters = []){
            try{
				
                $this->executeStatement($statement , $parameters);
				
            }catch(Exception $exception){
                throw new Exception($exception->getMessage());   
            }		
        }

		// Remove a row/s in a Database Table
        public function Remove( $statement = "" , $parameters = [] ){
            try{
				
                $this->executeStatement($statement , $parameters);
				
            }catch(Exception $exception){
                throw new Exception($exception->getMessage());   
            }		
        }     

        // execute statement
        private function executeStatement($statement = "", $parameters = []){
        	try{

        		$stmt = $this->connection->prepare($statement);
        		$stmt->execute($parameters);
        		return $stmt;

        	}catch(Exception $exception){
        		throw new Exception($exception->getMessage);
        		
        	}
        }   

 	}

    // connection to the base
    $db = new DatabaseClass();

 ?>