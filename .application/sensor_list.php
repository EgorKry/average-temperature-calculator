<!DOCTYPE html> 
<html lang="ru"> 
<head>	
	<meta charset="utf-8">
	<link rel="shortcut icon" href="https://cdn-icons.flaticon.com/png/512/2100/premium/2100130.png?token=exp=1641492935~hmac=a18513fa4f2cd6fd39f790b5128f6bca">
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<title>Список датчиков</title>
</head> 
<body> 
	<header class="bg-primary bg-gradient">
		<div class="container pb-4">
			<div class="row">
				<div class="col-12">
					<h1 class="text-center text-white pt-3">Средняя температура </h1>
				</div>
			</div>
		</div>
	</header>
		<div class="container">
			<div class="row">
				<menu class="col-xl-2 col-md-6 col-sm-12 pt-5 pb-5 bg-light fw-bold">
					<p class="border-bottom">Датчики</p>
					<p class="px-4 border-bottom bg-secondary text-white">Список датчиков</p>
					<p class="px-4 border-bottom"><a href="adding_sensor.php" class="link-dark text-decoration-none text-reset">Добавить датчик</a></p>
					<p class="border-bottom">Отчет</p>
					<p class="px-4 border-bottom"><a href="generate_report.php" class="link-dark text-decoration-none text-reset">Сформировать</a></p>
					<p class="px-4 border-bottom"><a href="report_history.php" class="link-dark text-decoration-none text-reset">История</a></p>
				</menu>
				<section class="col-xl-10 col-md-6 col-sm-12">
					<!-- Табличка с вываодом информации о датчиках -->
					<div class="row justify-content-center p-5">
						<div class="col-xl-8">
							<table class="table table-light table-striped caption-top">
								<caption class="bg-primary text-center h2 font-weight-bold text-white">Список датчиков</caption>
  								
  								<?php 
  								
  									include_once "connection.php";
									$array_of_sensors = $db->Select("Select * from SensorList");
								
									// array is empty
									if (empty($array_of_sensors)) {
										echo '
											<tr>
											<td>
											<center><b>Список датчиков пуст</b></center>
											</td>
											</tr>';
									} else { // array isn't empty
	
										echo '
											<thead>
    										<tr>
      										<th scope="col">Номер</th>
      										<th scope="col">Название</th>
     										<th scope="col">Единица измерения</th>
      										<th scope="col">Значение</th>
      										<th scope="col">Действия</th>				
    										</tr>
  											</thead>
  											<tbody>';

  										$number_of_sensor = 1;
  										foreach($db->Select("Select * from SensorList") as $sensor) {
 											echo "
 												<tr>
      											<th scope='row'>".$number_of_sensor."</th>
      											<td>".$sensor['name']."</td>
      											<td>".$sensor['unit']."</td>
      											<td>".$sensor['value']."</td>
      											<td class='align-middle'> 
      											<a href='remove_sensor.php?did=".$sensor['id']." '><img src='png/delete.png'></a>
      											<a href='update_sensor.php?id=".$sensor['id']." '><img src='png/update.png'></a>
												</td>
    											</tr>";
 								   			$number_of_sensor += 1;
										}
										echo'</tbody>';
									}
								
  								?>
							</table>
						</div>
					</div>
				</section>
			</div>
		</div>
	<footer class="fixed-bottom bg-secondary bg-gradient">
		<div class="container pb-5">	
		</div>
	</footer>
</body>
</html>