<?php

	include_once "connection.php";

	$name = htmlspecialchars($_POST['input_sensor_name']);
	$unit = htmlspecialchars($_POST['unit']);
	$value = htmlspecialchars($_POST['value']);

	$db->Insert("Insert into `SensorList`( `name` , `unit`, `value`) values (:name , :unit, :value)", [
        'name' => $name,
        'unit' => $unit,
        'value' => $value,
    	]);

	$new_url = 'http://localhost:14000/adding_sensor.php';
	header('Location: '.$new_url);
	exit();
?>