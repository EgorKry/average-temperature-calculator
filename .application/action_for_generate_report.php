<?php

	include_once "connection.php";

	$selected_unit = htmlspecialchars($_POST["unit"]);
	$today = date("Y-m-d");
	$sum = 0;
	$number_of_sensors = 0;

	foreach($db->Select("Select * from SensorList") as $sensor) {
		if ($selected_unit == 'Цельсий') {

			if ($sensor['unit'] == 'Фаренгейт') {
				$sum += (5/9) * ($sensor['value'] - 32);
			} else{
				$sum += $sensor['value'];
			}

		}else{

			if ($sensor['unit'] == 'Цельсий') {
				$sum += (9/5) * $sensor['value'] + 32;
			} else{
				$sum += $sensor['value'];
			}

		}
		$number_of_sensors += 1;
	}

	$mean = $sum / $number_of_sensors;
	$mean = round($mean, 1);


	$db->Insert("Insert into `reports`(`date`, `unit`, `mean`) values ( :date , :unit, :mean)", [
        'date' => $today,
        'unit' => $selected_unit,
        'mean' => $mean,
    ]);

	$new_url = 'http://localhost:14000/generate_report.php';
		header('Location: '.$new_url);
		exit();

?>