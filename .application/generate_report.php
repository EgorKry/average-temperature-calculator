<!DOCTYPE html> 
<html lang="ru"> 
<head>	
	<meta charset="utf-8">
	<link rel="shortcut icon" href="https://cdn-icons.flaticon.com/png/512/2100/premium/2100130.png?token=exp=1641492935~hmac=a18513fa4f2cd6fd39f790b5128f6bca">
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<title>Сформировать отчёт</title>
</head> 
<body> 
	<header class="bg-primary bg-gradient">
		<div class="container pb-4">
			<div class="row">
				<div class="col-12">
					<h1 class="text-center text-white pt-3">Средняя температура  </h1>
				</div>
			</div>
		</div>
	</header>
	<div class="container">
		<div class="row">
			<menu class="col-xl-2 col-md-6 col-sm-12 pt-5 pb-5 bg-light fw-bold">
				<p class="border-bottom">Датчики</p>
				<p class="px-4 border-bottom"><a href="sensor_list.php" class="link-dark text-decoration-none text-reset">Список датчиков</a></p>
				<p class="px-4 border-bottom"><a href="adding_sensor.php" class="link-dark text-decoration-none text-reset">Добавить датчик</a></p>
				<p class="border-bottom">Отчет</p>
				<p class="px-4 border-bottom bg-secondary text-white">Сформировать</p>
				<p class="px-4 border-bottom"><a href="report_history.php" class="link-dark text-decoration-none text-reset">История</a></p>
			</menu>
			<section class="col-xl-10 col-md-6 col-sm-12">
				<!-- Окно формирования отчёта -->
				<div class="row justify-content-center p-5">
					<div class="col-xl-8 border border-2 border-primary"> 
						<div class="row bg-primary border-bottom border-2 border-primary justify-content-center text-white fw-bold">
							Сформировать отчёт
						</div>
						<div class="row p-4">
						<!-- Форма формирования отчёта -->
						<form action="action_for_generate_report.php" method="POST">
							<!-- Выбор формата градусов -->
							<div class="mb-3">
								<label for="choose_format" class="form-label">Формат градусов:</label>
								<select class="form-select" aria-label="Выбор формата температуры" name="unit">
  									<option value="Цельсий">Цельсий</option>
  									<option value="Фаренгейт">Фаренгейт</option>
								</select>
							</div>
							<div class="text-center">
								<button type="submit" class="btn btn-primary">Сформировать</button>
							</div>
						</form>
						</div>
					</div>
				</div>
				<!-- Датчики -->
				<div class="row justify-content-center p-5">
					<div class="col-xl-8">
						<table class="table table-light table-striped caption-top">
							<caption class="bg-primary text-center h2 font-weight-bold text-white">Список датчиков</caption>
  							<thead>
    						<tr>
      						<th scope="col">Номер</th>
      						<th scope="col">Название</th>
     						<th scope="col">Единица измерения</th>
      						<th scope="col">Значение</th>				
    						</tr>
  							</thead>
  							<tbody>
    					
  							<?php 
  								include_once "connection.php";

								foreach($db->Select("Select * from SensorList") as $sensor) {
 									echo "
 									<tr>
      								<th scope='row'>".$sensor['id']."</th>
      								<td>".$sensor['name']."</td>
      								<td>".$sensor['unit']."</td>
      								<td>".$sensor['value']."</td>
    								</tr>";
								}
  							?>

  							</tbody>
						</table>
					</div>
				</div>
			</section>
		</div>
	</div>
	<footer class="footer fixed-bottom bg-secondary bg-gradient">
		<div class="container pb-5">	
		</div>
	</footer>
</body>
</html>