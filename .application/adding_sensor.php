<!DOCTYPE html> 
<html lang="ru"> 
<head>	
	<meta charset="utf-8">
	<link rel="shortcut icon" href="https://cdn-icons.flaticon.com/png/512/2100/premium/2100130.png?token=exp=1641492935~hmac=a18513fa4f2cd6fd39f790b5128f6bca">
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<title>Добавить датчик</title>
</head> 
<body> 
	<header class="bg-primary bg-gradient">
		<div class="container pb-4">
			<div class="row">
				<div class="col-12">
					<h1 class="text-center text-white pt-3">Средняя температура</h1>
				</div>
			</div>
		</div>
	</header>
		<div class="container">
			<div class="row">
				<menu class="col-xl-2 col-md-6 col-sm-12 pt-5 pb-5 bg-light fw-bold">
					<p class="border-bottom">Датчики</p>
					<p class="px-4 border-bottom"><a href="sensor_list.php" class="link-dark text-decoration-none text-reset">Список датчиков</a></p>
					<p class="px-4 border-bottom bg-secondary text-white">Добавить датчик</p>
					<p class="border-bottom">Отчет</p>
					<p class="px-4 border-bottom"><a href="generate_report.php" class="link-dark text-decoration-none text-reset">Сформировать</a></p>
					<p class="px-4 border-bottom"><a href="report_history.php" class="link-dark text-decoration-none text-reset">История</a></p>
				</menu>
				<section class="col-xl-10 col-md-6 col-sm-12">
					<!-- Окно для ввода информации -->
					<div class="row justify-content-center p-5">
						<div class="col-xl-8 border border-2 border-primary">
							<div class="row bg-primary border-bottom border-2 border-primary justify-content-center text-white fw-bold">
								Добавить датчик
							</div>
							<div class="row p-3">
								<!-- Форма добавления нового датчика -->
								<form class="row g-3 was-validated" action="action_for_adding.php" method="POST">
									<!-- Ввод названия датчика -->
									<div class="mb-3">
										<label for="input_sensor_name" class="form-label">Название датчика:</label>
										<input type="text" class="form-control" name="input_sensor_name" required>
										<div class="invalid-feedback">
    										Пожалуйста, введите название датчика.
    									</div>
									</div>
									<!-- Выбор единицы измерения -->
									<div class="mb-3">
										<label for="choose_format" class="form-label">Формат градусов:</label>
										<select class="form-select" aria-label="Выбор формата температуры" name="unit">
  											<option value="Цельсий">Цельсий</option>
  											<option value="Фаренгейт">Фаренгейт</option>
										</select>
									</div>
									<!-- Ввод значения температуры -->
									<div class="mb-3">
										<label for="input_sensor_name" class="form-label">Значение:</label>
										<input type="number" class="form-control" id="value" name="value" required>
										<div class="invalid-feedback">
    										Пожалуйста, введите целочисленное значение датчика.
    									</div>
									</div>
									<div class="text-center">
										<button type="submit" class="btn btn-primary">Добавить</button>
									</div>
								</form>

							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
	<footer class="fixed-bottom bg-secondary bg-gradient">
		<div class="container pb-5">
		</div>
	</footer>
</body>
</html>